package com.tkppensioen.harms.oca.opdr01;

import com.tkppensioen.harms.oca.opdr01.model.YathzeeSpel;

/**
 * this is the main runner of the program
 *
 * @author Henderikus Harms
 */
public class Main {

    public static void main(String[] args) {
        /* initiate a yathzee instance */
        YathzeeSpel game = new YathzeeSpel();

        /* start game */
        game.spelen();

        System.out.println("Bedankt voor het spelen!");
    }
}
