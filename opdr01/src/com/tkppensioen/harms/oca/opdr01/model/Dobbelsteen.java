package com.tkppensioen.harms.oca.opdr01.model;

import java.util.Random;

public class Dobbelsteen {
    static int MIN = 1;
    static int MAX = 6;

    private int laatsteWorp;

    public int getLaatsteWorp() {
        return laatsteWorp;
    }

    public int werpen() {
        this.laatsteWorp = new Random().nextInt(MAX) + MIN;
        return this.laatsteWorp;
    }
}
