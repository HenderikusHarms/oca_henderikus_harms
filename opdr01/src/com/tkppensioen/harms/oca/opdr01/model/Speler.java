package com.tkppensioen.harms.oca.opdr01.model;

import java.util.ArrayList;

/**
 * This claas stores the scores of a player
 *
 * @author Henderikus Harms
 */
public class Speler {
    private ArrayList<Worp> worpen;


    public Speler() {
        this.worpen = new ArrayList<>();
    }

    public void addWorp(Worp worp) {
        this.worpen.add(worp);
    }

    public ArrayList<Worp> getWorpen() {
        return this.worpen;
    }
}
