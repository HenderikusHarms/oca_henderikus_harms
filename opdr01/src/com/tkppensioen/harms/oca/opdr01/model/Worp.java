package com.tkppensioen.harms.oca.opdr01.model;

/**
 * this class stores the values of a turn
 *
 * @author Henderikus Harms
 */
public class Worp {
    private int worpNummer;
    private int[] resultaat;

    public Worp(int worpNummer) {
        this.worpNummer = worpNummer;
        this.resultaat = new int[YathzeeSpel.AMOUNT_DICES];
    }

    public int[] getResultaat() {
        return this.resultaat;
    }

    public int getWorpNummer() {
        return this.worpNummer;
    }

    /**
     * Print throws of dices for the current turn
     * @return
     */
    public String turnToString() {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n" + "WORP").append(worpNummer).append("\n");

        for (int i = 1; i <= this.resultaat.length; i++){
            stringBuilder.append(i).append(" ");
        }

        stringBuilder.append("\n");

        for(int i = 0; i < resultaat.length; i++) {
            stringBuilder.append(this.getResultaat()[i]).append(" ");
        }

        return stringBuilder.toString();
    }

}
