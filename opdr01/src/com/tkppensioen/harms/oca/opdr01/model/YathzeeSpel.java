package com.tkppensioen.harms.oca.opdr01.model;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class controls the actual game
 *
 * @author Henderikus Harms
 */
public class YathzeeSpel {
    static final int AMOUNT_DICES = 5;
    static int AMOUNT_RETRIES = 3;

    private int[] blockedDices;
    private ArrayList<Dobbelsteen> dobbelstenen;
    private Speler[] spelers;

    public YathzeeSpel() {
        this.dobbelstenen = new ArrayList<>();
        this.blockedDices = new int[AMOUNT_DICES];
        this.initPlayers();

        for (int i = 0; i < AMOUNT_DICES; i++) {
            this.dobbelstenen.add(new Dobbelsteen());
        }
    }

    /**
     * Set amount of player conform user input
     *
     * The assignment was max 2 so any input other than 1 or 2 will give an error messages
     */
    private void initPlayers() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Geef het aantal spelers door 1 of 2 in te typen: ");
        String input = scanner.next();

        try{
            int aantalSpelers = Integer.parseInt(input);

            if (aantalSpelers > 0 && aantalSpelers < 3 ){
                this.spelers = new Speler[aantalSpelers];

                for(int i = 0; i < aantalSpelers; i++) {
                    this.spelers[i] = new Speler();
                }
            } else {
                throw new Exception("Wrong input given. Please type 1 or 2");
            }
        } catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    /**
     * When q is entered return false else return true
     */
    private boolean continueGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Druk Enter voor het werpen van een nieuwe worp. Druk op q voor het stoppen met het spel: ");
        String userInput = scanner.nextLine();

        if (userInput.equals("q")) {
            return false;
        }

        return true;
    }

    /**
     * Keep throws conform user input.
     *
     * The position of the locked dices are stored in the array lockedDices
     */
    private void vasthouden() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welke posities wilt u vasthouden? 0 voor geen anders bv. 124\n");

        System.out.print("INVOER: ");
        String userInput = scanner.next();

        try {
            for(int i = 0; i < userInput.length(); i++){
                int value = Integer.parseInt(String.valueOf(userInput.charAt(i)));
                if(value > 0 && value <= this.dobbelstenen.size()) {
                    this.blockedDices[value - 1] = 1;
                }else if (value == 0) {
                    break;
                }else {
                    throw new Exception("Please enter a number from 0 to " + this.dobbelstenen.size());
                }
            }
        } catch (NumberFormatException e) {
            System.err.println("Invalid input type: please type a number from 0 to " + this.dobbelstenen.size());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Throw dices if not blocked
     * @param worp stores values if this turn
     * @param retry amount of retries in current turn
     */
    private void throwDices(Worp worp, int retry) {
        /* throw each dice */
        for(Dobbelsteen dobbelsteen : dobbelstenen) {

            if (retry >= 1) {
                if (this.blockedDices[this.dobbelstenen.indexOf(dobbelsteen)] == 0) {
                    dobbelsteen.werpen();
                }
            } else {
                dobbelsteen.werpen();
            }
            worp.getResultaat()[this.dobbelstenen.indexOf(dobbelsteen)] = dobbelsteen.getLaatsteWorp();
        }
        /* show result of the dices */
        System.out.println(worp.turnToString() + "\n");
    }

    /**
     * play yathzee until q is pressed
     */
    public void spelen() {
        int worpNummer = 1;
        do {
            for(int i = 0; i < spelers.length; i++) {
                Speler speler = spelers[i];
                System.out.println("Het is de beurt aan speler: " + i);

                Worp worp = new Worp(worpNummer);

                /* A player is allow to rethrow three times conform the rules*/
                for(int retries = 0;  retries < AMOUNT_RETRIES; retries++) {
                    this.throwDices(worp, retries);
                    this.vasthouden();
                }

                /* reset block dices for next player or turn*/
                this.blockedDices = new int[AMOUNT_DICES];

                /* save turn af retries */
                speler.addWorp(worp);
            }
            worpNummer++;
        } while(this.continueGame());

        System.out.println("Jammer");
    }
}
